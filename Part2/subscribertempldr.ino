/* 
 * rosserial Subscriber for Temp and LDR Values from data published by python node in rostopic 'status1'
 *  This subscribes to topic 'status1' and based on the strings received, it cause led and buzzer output
 */

#include <ros.h>
#include <std_msgs/String.h>

ros::NodeHandle  nh;

void messageCb( const std_msgs::String& status1){

   //if readings are warning, go to warning function
   char *output = NULL;
   output = strstr (status1.data,"warning");
   if(output) {
      warning(1000);

   }

   //if readings are dangerous go to beep function
   char *output1 = NULL;
   output1 = strstr (status1.data,"danger");
   if(output1) {
      beep(1000);

   }


}

//Inititae Subscriber
ros::Subscriber <std_msgs::String> sub("status1", &messageCb );

//Inititae Variables
const int led1 = 12;
const int led2 = 6;
const int buzzerPin = 9;

void setup(){ 
  //Call subscriber
  nh.initNode();
  nh.subscribe(sub);
  //Serial.begin(57600);
  pinMode(buzzerPin, OUTPUT); //addigning pin to Output mode
  pinMode(led1, OUTPUT); //adding pin to output mode
  pinMode(led2, OUTPUT); //adding led 2 to output mode
  
}

void loop(){  
  nh.spinOnce();
  delay(1);
}


//this funtion lights blue led and the buzzer when the levels for both temp and light are dangerous
void beep(unsigned char delayms){

    digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(buzzerPin,HIGH);
    
    delay(delayms);          // wait for a delayms ms
  
    digitalWrite(buzzerPin,LOW);
    digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW


    delay(delayms);          // wait for a delayms ms   
} 

//this function lights green led as a warning when either temp or light values have increased to a dangerous level
void warning(unsigned char delayms){

    digitalWrite(6, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(delayms);          // wait for a delayms ms
  

    digitalWrite(6, LOW);    // turn the LED off by making the voltage LOW
    delay(delayms);          // wait for a delayms ms   
} 
 
