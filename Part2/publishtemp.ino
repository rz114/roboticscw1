/*
 * rosserial Publisher for Temperature and Light from Sensors
 * Publishes values from thermistor and ldr to topic named "chatter"
 */

#include <ros.h>
#include <std_msgs/String.h>
ros::NodeHandle  nh;

std_msgs::String str_msg;

//Declare Publisher
ros::Publisher chatter("chatter", &str_msg);

//Declare variables
char temp[10];  //temp value to be published
char light[10]; //light value to be published  
int ThermistorPin = A2;  // Thermistor Reading pin 
int sensorPin = A0; // select the input pin for LDR
int sensorValue = 0; // variable to store the value coming from the sensor


void setup(){
  //
  //pinMode(13, OUTPUT);
  //Inititate Subscriber
  nh.initNode();
  nh.advertise(chatter);
  //nh.subscribe(sub);

  //Initiate Output pins
  pinMode(buzzerPin, OUTPUT); //addigning buzzer pin to Output mode
  pinMode(led1, OUTPUT);  //led1 output
  pinMode(led2, OUTPUT);  //led2 putput
  
}

void loop()
{
    int Vo;   // Integer value of voltage reading  
  
    float R = 9870.0;   // Fixed resistance in the voltage divider  
    float logRt,Rt,T;  
    float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;  
   
    Vo = analogRead(ThermistorPin);   // Reading voltage Thermistor Reading pin  

    //////temperature conversion////////
    Rt = R*( 1023.0 / (float)Vo - 1.0 );  
    logRt = log(Rt);  
    T = ( 1.0 / (c1 + c2*logRt + c3*logRt*logRt*logRt ) ) - 273.15; 
    //////////////////////////////////////

    sensorValue = analogRead(sensorPin); //read ldr reading pin

       //convert to char
       dtostrf(T, 6, 2, temp);
       dtostrf(sensorValue, 6, 2, light); 
       char values[15];
       
       //concat both values together
       strcpy(values, temp);
       strcat(values, ",");
       strcat(values, light);
      
       //save it in string to be oublished
       str_msg.data = values;
      
       //publish the values
       chatter.publish( &str_msg );
       nh.spinOnce();
    
 delay(1000);

}
