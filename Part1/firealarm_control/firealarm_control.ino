 /*  
   
Created 28/10/2018  
   
 Components:  
 * Arduino UNO  
 * Thermistor  
 * 10K resistor  
 * LDR  
 * 10K resistor  
   
 - Gets Temperature form a Thermistor  
   
 - Gets an analogue value of Light from an LDR  
   
 Prints the data to the Serial Monitor  
   
 */  

const int led1 = 12;
const int led2 = 6;

const int buzzerPin = 9;

int sensorPin = A0; // select the input pin for LDR
int ThermistorPin = A2;  // Thermistor Reading pin  
int sensorValue = 0; // variable to store the value coming from the sensor

void setup() {
 
    Serial.begin(9600); //sets serial port for communication
    pinMode(buzzerPin, OUTPUT); //addigning pin to Output mode
    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
}

void loop() {
  
    int Vo;   // Integer value of voltage reading  
  
    float R = 9870.0;   // Fixed resistance in the voltage divider  
    float logRt,Rt,T;  
    float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;  
   
    Vo = analogRead(ThermistorPin);   // Reading voltage Thermistor Reading pin  
 
    Rt = R*( 1023.0 / (float)Vo - 1.0 );  
    logRt = log(Rt);  
    T = ( 1.0 / (c1 + c2*logRt + c3*logRt*logRt*logRt ) ) - 273.15; 
    
    sensorValue = analogRead(sensorPin); // read the value from the sensor
    Serial.print(sensorValue); //prints the values coming from the sensor on the screen
    Serial.print("          "); 
    Serial.println(T); //prints the values coming from the sensor on the screen

    delay(500);

    if(T > 33.5 and sensorValue>1010 ){

        beep(5000);
        
        //tone(buzzerPin, 50);
    }
    if((T > 33.5 and sensorValue<1010) or (T<33.5 and sensorValue>1010)){

          warning(5000);


     }
    delay(100);

}

void beep(unsigned char delayms){

    digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(buzzerPin,HIGH);
    
    delay(delayms);          // wait for a delayms ms
  
    digitalWrite(buzzerPin,LOW);
    digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW


    delay(delayms);          // wait for a delayms ms   
} 

void warning(unsigned char delayms){

    digitalWrite(6, HIGH);   // turn the LED on (HIGH is the voltage level)

    delay(delayms);          // wait for a delayms ms
  

    digitalWrite(6, LOW);    // turn the LED off by making the voltage LOW

    delay(delayms);          // wait for a delayms ms   
} 
