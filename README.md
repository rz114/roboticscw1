Instructions for Good Pass: Part 2 folder code

Terminal 1
roscore

Terminal 2
source ~/catkin_ws/devel/setup.bash
rosrun rosserial_python serial_node.py /dev/ttyUSB0

Terminal 3
source ~/catkin_ws/devel/setup.bash
rostopic list
rostopic echo chatter

Terminal 4 
source ~/catkin_ws/devel/setup.bash
rosrun beginner_tutorials firealarm_listener.py

Terminal 5
source ~/catkin_ws/devel/setup.bash
rostopic echo status1

Terminal 6
source ~/catkin_ws/devel/setup.bash
rosrun rosserial_python serial_node.py /dev/ttyACM0

Excellent Pass Attempt:

Terminal 1
cd ~/catkin_ws
source devel/setup.bash
roscore

Terminal 2
cd ~/catkin_ws
source devel/setup.bash
 roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch

Terminal 3
cd ~/catkin_ws
source devel/setup.bash
rosrun rosserial_python serial_node.py /dev/ttyUSB0 __name:=node1

Terminal 4 
rostopic list
rostopic echo cmd_vel

Terminal 4
cd ~/catkin_ws
source devel/setup.bash
rosrun rosserial_python serial_node.py /dev/ttyACM0 __name:=node2


