/* 
 * rosserial Publishe to cmd_vel topic to move turtlebot forward and backward depending on fore warning and danger threat
 *  This is to move turtlebot3 in gazebo simulation
 */



#include <ArduinoHardware.h>
#include <ros.h>
#include <geometry_msgs/Twist.h>

ros::NodeHandle nh;

geometry_msgs::Twist msg;

ros::Publisher pub("cmd_vel", &msg);

int ThermistorPin = A2;  // Thermistor Reading pin 
int sensorPin = A0; // select the input pin for LDR
int sensorValue = 0; // variable to store the value coming from the sensor

void setup(){

 //Initiate Subscriber 
 nh.initNode();
 nh.advertise(pub);
} 

void loop(){
    pinMode(buzzerPin, OUTPUT); //addigning pin to Output mode
    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    int Vo;   // Integer value of voltage reading  
  
    float R = 9870.0;   // Fixed resistance in the voltage divider  
    float logRt,Rt,T;  
    float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;  
   
    Vo = analogRead(ThermistorPin);   // Reading voltage Thermistor Reading pin  
 
    Rt = R*( 1023.0 / (float)Vo - 1.0 );  
    logRt = log(Rt);  
    T = ( 1.0 / (c1 + c2*logRt + c3*logRt*logRt*logRt ) ) - 273.15; 
    sensorValue = analogRead(sensorPin); 
  
    if(T > 33.5 and sensorValue>1010 ){
      
       //move turtlebot forward
        msg.linear.x=-0.7;
      
     } else if((T > 33.5 and sensorValue<1010) or (T<33.5 and sensorValue>1010)){

       //move turtlebot backword slowly 
       msg.linear.x=0.3;

      
     } else{

         //don't move turtle bot
          msg.linear.x=0;
     }
 pub.publish(&msg);
 nh.spinOnce();
 }
