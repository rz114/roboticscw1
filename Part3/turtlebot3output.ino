 /* rosserial Subscriber to cmd_vel to light leds and buzzers depending on fire threat when turtlebot3 moves
 * This subscriber to cmd_vel sent to turtlebot3 gazebo simulation
 */

#include <ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>

ros::NodeHandle  nh;

void messageCb( const geometry_msgs::Twist& cmd_vel){
  //Serial.println(status1.data);
  //status1.data = input;
  //char copy[10];
  //status1.c_str(); 
  if(cmd_vel.linear.x < 0){
    beep(1000);
   }
   if(cmd_vel.linear.x > 0){
    warning(1000);
   }
   

}

ros::Subscriber <geometry_msgs::Twist> sub("cmd_vel", &messageCb );
const int led1 = 12;
const int led2 = 6;
const int buzzerPin = 9;
void setup(){ 
  nh.initNode();
  nh.subscribe(sub);
  //Serial.begin(57600);
  pinMode(buzzerPin, OUTPUT); //addigning pin to Output mode
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  
}

void loop(){  
  nh.spinOnce();
  delay(1);
}

void beep(unsigned char delayms){

    digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(buzzerPin,HIGH);
    
    delay(delayms);          // wait for a delayms ms
  
    digitalWrite(buzzerPin,LOW);
    digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW


    delay(delayms);          // wait for a delayms ms   
} 

void warning(unsigned char delayms){

    digitalWrite(6, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(delayms);          // wait for a delayms ms
  

    digitalWrite(6, LOW);    // turn the LED off by making the voltage LOW
    delay(delayms);          // wait for a delayms ms   
} 
 
